package edu.svsu.csis.roshambo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.lang.reflect.GenericArrayType;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnRock, btnPaper, btnScissors;
    String playerChoice, computerChoice;
    int userThrow = 0;
    int rock;
    int paper;
    int scissors;
    int[] computerThrows = new int[]{
            rock,
            paper,
            scissors
    };





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRock = (Button)findViewById(R.id.btnRock);
        btnPaper = (Button)findViewById(R.id.btnPaper);
        btnScissors = (Button)findViewById(R.id.btnScissors);



        btnRock.setOnClickListener(this);
        btnPaper.setOnClickListener(this);
        btnScissors.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.btnRock:
                userThrow = 1;
                computerThrow();
                break;
            case R.id.btnPaper:
                userThrow = 2;
                computerThrow();
                break;
            case R.id.btnScissors:
                userThrow = 3;
                computerThrow();
                break;
        }
    }


    private void computerThrow(){
        int roll = (int)(Math.random() * computerThrows.length);
        setWinner(roll);
    }



    private void setWinner(int roll){
        if (userThrow == 0){
            showResult(0);
        } else if (userThrow == 1 && roll == 0){ //User rock, computer rock
            showResult(1);
        } else if (userThrow == 1 && roll == 1){ // user rock, computer paper
            showResult(2);
        } else if (userThrow == 1 && roll == 2){ // user rock, computer scissors
            showResult(3);
        } else if (userThrow == 2 && roll == 0){ // user paper, computer rock
            showResult(4);
        } else if (userThrow == 2 && roll == 1){ // user paper, computer paper
            showResult(5);
        } else if (userThrow == 2 && roll == 2) { // user paper, computer scissors
            showResult(6);
        } else if (userThrow == 3 && roll == 0){ // user scissors, computer rock
            showResult(7);
        } else if (userThrow == 3 && roll == 1){ // user scissors, computer paper
            showResult(8);
        } else if (userThrow == 3 && roll == 2){ // user scissors, computer scissors
            showResult(9);
        }
    }

    private void showResult(int result){
        if (result == 0){
            Toast.makeText(getApplicationContext(), "Please make a selection!", Toast.LENGTH_SHORT).show(); // no selection
        } else if (result == 1){
            Toast.makeText(getApplicationContext(), "Rock ties Rock, Tie!", Toast.LENGTH_SHORT).show(); // rock v rock tie
        } else if (result == 2){
            Toast.makeText(getApplicationContext(), "Paper covers Rock, you lose!", Toast.LENGTH_SHORT).show();
        } else if (result == 3){
            Toast.makeText(getApplicationContext(), "Rock beats Scissors, you win!", Toast.LENGTH_SHORT).show();
        } else if (result == 4){
            Toast.makeText(getApplicationContext(), "Paper covers Rock, you win!", Toast.LENGTH_SHORT).show();
        } else if (result ==5){
            Toast.makeText(getApplicationContext(), "Paper ties Paper, tie!", Toast.LENGTH_SHORT).show();
        } else if (result == 6){
            Toast.makeText(getApplicationContext(), "Scissors beat Paper, you lose!", Toast.LENGTH_SHORT).show();
        } else if (result == 7){
            Toast.makeText(getApplicationContext(), "Rock beats Scissors, you lose!", Toast.LENGTH_SHORT).show();
        } else if (result == 8){
            Toast.makeText(getApplicationContext(), "Scissors beat Paper, you win!", Toast.LENGTH_SHORT).show();
        } else if (result == 9){
            Toast.makeText(getApplicationContext(), "Scissors tie Scissors, tie!", Toast.LENGTH_SHORT).show();
        }
    }
}

